Fedora Infrastructure
=====================

Welcome! This is the Fedora Infrastructure Pagure project. 

This project is mainly used as ticket tracker for Fedora Infrastructure.

If you want to know how the tickets are processed by Fedora Infrastructure Team
look at:

https://docs.fedoraproject.org/en-US/infra/sysadmin_guide/tickets/

Git repo of this project is misc scripts and tools for Fedora

If you are looking for the Fedora Infrastructure ansible repo, 
that is not here, look at: 

https://pagure.io/fedora-infra/ansible.git

If you would like to help out with Fedora Infrastructure, 
see: 

https://fedoraproject.org/wiki/Infrastructure/GettingStarted
and
https://fedoraproject.org/wiki/Infrastructure_Apprentice

For more info how to communicate with Fedora Infra Team, see:

https://docs.fedoraproject.org/en-US/cpe/day_to_day_fedora/


Ticket priorities explained
---------------------------

The tickets in Fedora Infrastructure have Priority field which on this project
isn't used for priority and instead it's used for ticket workflow. Following
is the description of each priority:

* URGENT

  This means that this ticket defines fire (something critical doesn't work) and
  should be resolved ASAP.

* Needs Review

  Default priority assigned to new ticket. This means that ticket is waiting for
  review from Fedora Infrastructure Team.

  .. note:: We sometimes forgot to change the priority from default state.

* Next meeting

  Ticket will be discussed on next meeting.

* Waiting on Assignee

  Ticket is waiting on somebody to take it. If somebody is already assigned than
  ticket is waiting for the assignee to finish the work.

* Waiting on Reporter

  Ticket is waiting for reply from reporter. This is used to clarify some information
  about the ticket or validation from reporter.

* Waiting on External

  Ticket is waiting for work that needs to be done outside Fedora Infrastructure
  team and which the team usually couldn't influence. For example waiting for
  hardware replacement.
